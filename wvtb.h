/*
Licensed CC0 (public domain worldwide)
2022 David DiPaola
*/

#ifndef __WVTB_H
#define __WVTB_H

void
wvtb_init(uint32_t sampleRate);

void
wvtb_ch_setWave(uint8_t ch, const int16_t * wave);

void
wvtb_ch_setPeriod(uint8_t ch, uint16_t period);

void
wvtb_ch_setVolume(uint8_t ch, uint8_t volume);

void
wvtb_ch_setEnable(uint8_t ch, _Bool enable);

void
wvtb_mix(int16_t * stream, size_t stream_len);

#endif
