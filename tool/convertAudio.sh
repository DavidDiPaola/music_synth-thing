$#/bin/bash

set -euo pipefail

INPUT="$1"

ffmpeg \
	-i "$INPUT" \
	-vn \
	-ac 1 -f s16le -acodec pcm_s16le \
	"$INPUT.raw" \
;
