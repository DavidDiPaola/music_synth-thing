#!/bin/bash

set -eu

if [ $# -lt 1 ] ; then
	echo "dumps 16bit audio data from a raw audio file" 1>&2
	echo "$(basename $0) <file>" 1>&2
	exit 1
fi
ARG_FILE="$1"

od -Ax -t x2 --endian=little -w1 -v "$ARG_FILE"
