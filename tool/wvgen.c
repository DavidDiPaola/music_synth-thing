#include <stdio.h>

#include <math.h>

#include <inttypes.h>

int
main() {
	for (int i=0; i<32; i++) {
		int16_t value = (int16_t)round(cos(2.0*3.1415*((double)i/32.0)) * INT16_MAX);
		printf("%i ", value);
	}
	printf("\n");

	return 0;
}
