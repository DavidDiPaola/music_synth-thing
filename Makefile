# Licensed CC0 (public domain worldwide)
# 2022 David DiPaola

SRC_C = main.c sqr.c wvtb.c
BIN = synth-thing

_CFLAGS = \
	-std=c99 -fwrapv \
	-Wall -Wextra \
	-g -pg \
	$(shell sdl-config --cflags) \
	$(CFLAGS)
_LDFLAGS = \
	-pg \
	$(LDFLAGS)
_LDFLAGS_LIB = \
	$(shell sdl-config --libs) -lSDL_mixer \
	-lm

OBJ = $(SRC_C:.c=.o)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)

.PHONY: prof
prof:
	gprof $(BIN) gmon.out

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) $^ -o $@ $(_LDFLAGS_LIB)
