/*
Licensed CC0 (public domain worldwide)
2022 David DiPaola
*/

#include <stdint.h>

#include <stdio.h>

#include <SDL.h>

#include <SDL_mixer.h>

#include "sqr.h"

#include "wvtb.h"

struct {
} _sdl;

#define SQR
//#define WVTB

static void
_audio_fill(void * udata, Uint8 * stream, int len) {
	(void)udata;
	Sint16 * stream16 = (void *)stream;
	size_t stream16_len = len / (sizeof(*stream16) / sizeof(*stream));

	#ifdef SQR
	sqr_mix(stream16, stream16_len);
	#endif

	#ifdef WVTB
	wvtb_mix(stream16, stream16_len);
	#endif
}

static int
_init(void) {
	int status;

	SDL_Init(SDL_INIT_AUDIO);

	SDL_AudioSpec wantedAudio = {
		.freq = 44100,
		.format = AUDIO_S16,
		.channels = 1,
		.samples = 1024,
		.callback = _audio_fill,
		.userdata = NULL
	};
	status = SDL_OpenAudio(&wantedAudio, NULL);
	if (status < 0) {
		fprintf(stderr, "ERROR: SDL_OpenAudio(): %s\n", SDL_GetError());
		return -1;
	}

	return 0;
}

static void
_close(void) {
	SDL_CloseAudio();

	SDL_Quit();
}

int
main() {
	int status;

	status = _init();
	if (status < 0) {
		return 1;
	}

	#ifdef SQR
	sqr_init();
	sqr_ch_setHlfPrd(0, 84);  /* C4 */
	sqr_ch_setVol(0, 255);
	sqr_ch_setEn(0, 1);
	sqr_ch_setHlfPrd(1, 67);  /* G4 */
	sqr_ch_setVol(1, 255);
	sqr_ch_setEn(1, 1);
	sqr_ch_setHlfPrd(2, 56);  /* E4 */
	sqr_ch_setVol(2, 255);
	sqr_ch_setEn(2, 1);
	#endif

	#ifdef WVTB
	wvtb_init(44100);
	#if 0
	wvtb_ch_setWave(0, (int16_t[]){
		 32767,  32767,  32767,  32767,  32767,  32767,  32767,  32767,
		 32767,  32767,  32767,  32767,  32767,  32767,  32767,  32767,
		-32768, -32768, -32768, -32768, -32768, -32768, -32768, -32768,
		-32768, -32768, -32768, -32768, -32768, -32768, -32768, -32768
	});
	#else
	wvtb_ch_setWave(0, (int16_t[]){
		 32767,  32137,  30273,  27245,  23170,  18205,  12540,   6394,
		     2,  -6391, -12538, -18203, -23168, -27243, -30272, -32137,
		-32767, -32138, -30274, -27247, -23172, -18208, -12543,  -6397,
		    -5,   6388,  12535,  18200,  23166,  27242,  30271,  32136
	});
	#endif
	wvtb_ch_setPeriod(0, 1);
	wvtb_ch_setVolume(0, 255);
	wvtb_ch_setEnable(0, 1);
	#endif

	SDL_PauseAudio(0);

	int quit = 0;
	while (!quit) {
		SDL_Event event;
		while (!SDL_PollEvent(&event)) {
			SDL_Delay(16);

			if (SDL_GetTicks() > 2000) {
				quit = 1;
				break;
			}
		}
		if (event.type == SDL_QUIT) {
			quit = 1;
			break;
		}
	}

	_close();
	return 0;
}

