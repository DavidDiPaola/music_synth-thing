/*
Licensed CC0 (public domain worldwide)
2022 David DiPaola
*/

#include <stdint.h>

#include <string.h>

#define _state_ch_LEN 4

struct _state_ch {
	uint16_t counter;
	uint16_t halfPeriod;
	uint8_t  volume;
	_Bool    enable;
};

static struct {
	struct _state_ch ch[_state_ch_LEN];
} _state;

void
sqr_init(void) {
	memset(&_state.ch, 0, sizeof(_state.ch));
}

void
sqr_ch_setHlfPrd(uint8_t ch, uint16_t halfPeriod) {
	_state.ch[ch].halfPeriod = halfPeriod;
}

void
sqr_ch_setVol(uint8_t ch, uint8_t volume) {
	_state.ch[ch].volume = volume;
}

void
sqr_ch_setEn(uint8_t ch, _Bool enable) {
	_state.ch[ch].enable = enable;
	_state.ch[ch].counter = 0;
}

size_t
min_size_t(size_t a, size_t b) {
	return (a < b) ? a : b;
}

void
sqr_mix(int16_t * stream, size_t stream_len) {
	memset(stream, 0, stream_len*sizeof(*stream));

	for (size_t i=0; i<_state_ch_LEN; i++) {
		struct _state_ch * ch = _state.ch + i;
		if ( !(ch->enable) || (ch->volume == 0) ) {
			continue;
		}

		for (size_t stream_i=0; stream_i<stream_len; ) {
			size_t stream_remain = stream_len - stream_i;

			size_t fill_len;
			size_t fill_val = (INT16_MAX / (UINT8_MAX / ch->volume)) / _state_ch_LEN;
			if (ch->counter < ch->halfPeriod) {
				fill_len = min_size_t( ch->halfPeriod - ch->counter, stream_remain );
			}
			else {
				fill_len = min_size_t( (ch->halfPeriod * 2) - ch->counter, stream_remain );
				fill_val *= -1;
			}

			for (size_t j=0; j<fill_len; j++,stream_i++) {
				stream[stream_i] += fill_val;
			}
			ch->counter = (ch->counter + fill_len) % (ch->halfPeriod * 2);
		}
	}
}
