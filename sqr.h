/*
Licensed CC0 (public domain worldwide)
2022 David DiPaola
*/

#include <stdint.h>

void
sqr_init(void);

/*
Sets a channel's frequency using a half period value.
half period = (sample rate / desired frequency) / 2
Example half period values at 44100hz and error percentages:
	802:   27.5hz (A0 -0.1%)
	 84:  262.5hz (C4/middle C +0.3%)
	  5: 4410.0hz (C8 +5%)
*/
void
sqr_ch_setHlfPrd(uint8_t ch, uint16_t halfPeriod);

void
sqr_ch_setVol(uint8_t ch, uint8_t volume);

void
sqr_ch_setEn(uint8_t ch, _Bool enable);

void
sqr_mix(int16_t * stream, size_t stream_len);
