/*
Licensed CC0 (public domain worldwide)
2022 David DiPaola
*/

#include <stdint.h>

#include <string.h>

#define _state_ch_wave_LEN 32
#define _state_ch_LEN 4

struct _state_ch {
	int16_t   wave[_state_ch_wave_LEN];
	size_t    wave_i;
	uint16_t  period;
	uint8_t   volume;
	_Bool     enable;
};

static struct {
	uint32_t sampleRate;
	struct _state_ch ch[_state_ch_LEN];
} _state;

void
wvtb_init(uint32_t sampleRate) {
	_state.sampleRate = sampleRate;
	memset(&_state.ch, 0, sizeof(_state.ch));
}

void
wvtb_ch_setWave(uint8_t ch, const int16_t * wave) {
	memcpy(_state.ch[ch].wave, wave, sizeof(_state.ch[ch].wave));
}

void
wvtb_ch_setPeriod(uint8_t ch, uint16_t period) {
	_state.ch[ch].period = period;
}

void
wvtb_ch_setVolume(uint8_t ch, uint8_t volume) {
	_state.ch[ch].volume = volume;
}

void
wvtb_ch_setEnable(uint8_t ch, _Bool enable) {
	_state.ch[ch].enable = enable;
}

void
wvtb_mix(int16_t * stream, size_t stream_len) {
	memset(stream, 0, stream_len*sizeof(*stream));

	for (size_t i=0; i<_state_ch_LEN; i++) {
		struct _state_ch * ch = _state.ch + i;
		if ( (!ch->enable) || (ch->volume == 0) ) {
			continue;
		}

		int16_t * wave   = ch->wave;
		size_t    wave_i = ch->wave_i;
		for (size_t j=0; j<stream_len; j++) {
			stream[j] += (wave[wave_i] / (UINT8_MAX / ch->volume)) / _state_ch_LEN;  /* TODO use period and wave_i */

			if ( (ch->period == 0) || ((j % ch->period) == 0) ) {
				wave_i = (wave_i + 1) % _state_ch_wave_LEN;
			}
		}
	}
}
